import json


class YunPian(object):

    def __init__(self, api_key):
        self.api_key = api_key
        self.single_send_url = "xxx"

    def send_sms(self, code, mobile):
        params = {
            "apikey" : self.api_key,
            "mobile": mobile,
            "text": "【慕学生鲜】您的验证码是{code}。如非本人操作，请忽略本短信".format(code=code)
        }
        res = {"code":0, "msg":"发送成功"}
        res_dict = json.dumps(res)
        res_dict = json.loads(res_dict)
        return res_dict