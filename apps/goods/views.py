from .serializers import GoodsSerializers
from rest_framework import mixins, generics
from rest_framework.pagination import PageNumberPagination
from rest_framework import viewsets, filters

# 给每个视图函数，单独设置是否需要toekn
# from rest_framework.authentication import TokenAuthentication

from django_filters.rest_framework import DjangoFilterBackend


from apps.goods.models import Goods, GoodsCategory
from apps.goods.filters import GoodsFilter
from apps.goods.serializers import GoodsSerializers, CategorySerializers
# Create your views here.


class GoodsPagination(PageNumberPagination):
    page_size = 10
    page_size_query_param = 'page_size'
    page_query_param = 'page'
    max_page_size = 100


class GoodsListViewSet(mixins.ListModelMixin, mixins.RetrieveModelMixin, viewsets.GenericViewSet):
    """
    List all snippets, or create a new snippet.
    """
    queryset = Goods.objects.all()
    serializer_class = GoodsSerializers
    pagination_class = GoodsPagination

    # 给每个视图函数，单独设置是否需要toekn
    # authentication_classes = (TokenAuthentication, )

    filter_backends = (DjangoFilterBackend, filters.SearchFilter, filters.OrderingFilter)
    filter_fields = ("name", "shop_price")
    filter_class = GoodsFilter
    search_fields = ('name', 'goods_brief', 'goods_desc')
    ordering_fields = ('sold_num', 'add_time')


class CategoryViewSet(mixins.ListModelMixin, mixins.RetrieveModelMixin, viewsets.GenericViewSet):
    '''
    list：
        商品分类列表
    retrieve:
        获取商品分类详情
    '''
    queryset = GoodsCategory.objects.all()
    serializer_class = CategorySerializers

