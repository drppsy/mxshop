from rest_framework import serializers

from apps.goods.models import Goods, GoodsCategory, GoodsImage


class CategorySerializers3(serializers.ModelSerializer):
    class Meta:
        model = GoodsCategory
        # fields = ("name", "click_num", "market_price", "add_time")
        fields = "__all__"


class CategorySerializers2(serializers.ModelSerializer):
    sub_cat = CategorySerializers3(many=True)
    class Meta:
        model = GoodsCategory
        # fields = ("name", "click_num", "market_price", "add_time")
        fields = "__all__"


class CategorySerializers(serializers.ModelSerializer):
    sub_cat = CategorySerializers2(many=True)
    class Meta:
        model = GoodsCategory
        # fields = ("name", "click_num", "market_price", "add_time")
        fields = "__all__"


class GoodsImageSerializer(serializers.ModelSerializer):

    class Meta:
        model = GoodsImage
        fields = ("image", )


class GoodsSerializers(serializers.ModelSerializer):
    category = CategorySerializers()
    images = GoodsImageSerializer(many=True)

    class Meta:
        model = Goods
        # fields = ("name", "click_num", "market_price", "add_time")
        fields = "__all__"