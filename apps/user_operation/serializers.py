import re
from rest_framework import serializers
from rest_framework.validators import UniqueTogetherValidator

from apps.user_operation.models import UserFav, UserLeavingMessage, UserAddress
from apps.goods.serializers import GoodsSerializers

from MxShop.settings import REGEX_MOBILE


class UserFavDetailSerializer(serializers.ModelSerializer):
    goods = GoodsSerializers()

    class Meta:
        model = UserFav
        fields = ("goods", "id")


class UserFavSerializer(serializers.ModelSerializer):
    user = serializers.HiddenField(
        default=serializers.CurrentUserDefault()
    )

    class Meta:
        model = UserFav
        validators = [
            UniqueTogetherValidator(
                queryset=UserFav.objects.all(),
                fields=['user', 'goods'],
                message="已经收藏该商品了哦"
            )
        ]

        fields = ("user", "goods", "id")


class LeavingMessageSerializer(serializers.ModelSerializer):
    user = serializers.HiddenField(
        default=serializers.CurrentUserDefault()
    )

    add_time = serializers.DateTimeField(read_only=True, format="%Y-%m-%d %H:%M")

    class Meta:
        model = UserLeavingMessage

        fields = ("user", "message_type", "subject", "message", "file", "id", "add_time")


class AddressSerializer(serializers.ModelSerializer):
    user = serializers.HiddenField(
        default=serializers.CurrentUserDefault()
    )

    province = serializers.CharField(required=True, min_length=2, max_length=5, help_text="省")
    city = serializers.CharField(required=True, min_length=2, max_length=5, help_text="城市")
    district = serializers.CharField(required=True, min_length=2, max_length=10, help_text="区")
    address = serializers.CharField(required=True, min_length=2, max_length=20, help_text="详细地址")
    signer_name = serializers.CharField(required=True, min_length=2, max_length=15, help_text="收货人")
    signer_mobile = serializers.CharField(required=True, min_length=11, max_length=11, help_text="手机号")
    add_time = serializers.DateTimeField(read_only=True, format="%Y-%m-%d %H:%M")

    def validate_signer_mobile(self, signer_mobile):
        if not re.match(REGEX_MOBILE, signer_mobile):
            raise serializers.ValidationError("请输入正确的手机号")
        return signer_mobile

    class Meta:
        model = UserAddress

        fields = ("id", "user", "province", "city", "district", "address", "signer_name", "signer_mobile", "add_time")
