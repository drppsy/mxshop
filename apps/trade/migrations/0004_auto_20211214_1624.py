# Generated by Django 2.2 on 2021-12-14 16:24

from django.conf import settings
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('goods', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('trade', '0003_auto_20211214_1622'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='ShoppingCard',
            new_name='ShoppingCart',
        ),
    ]
