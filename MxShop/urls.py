"""MxShop URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
# from django.contrib import admin
from django.conf.urls import url,include
from django.urls import path,re_path
import xadmin
from MxShop.settings import MEDIA_ROOT
from django.views.static import serve
from rest_framework.documentation import include_docs_urls
from rest_framework.routers import DefaultRouter
from rest_framework.authtoken import views
from rest_framework_jwt.views import obtain_jwt_token


from apps.goods.views import GoodsListViewSet, CategoryViewSet
from apps.users.views import SmsCodeViewSet, UserViewSet
from apps.user_operation.views import UserFavViewset, LeavingMessageViewset, AddressViewset
from apps.trade.views import ShoppingCartVeiwset, OrderViewset

router = DefaultRouter()
# 配置goods的url
router.register(r'goods', GoodsListViewSet, basename='common')

# 配置categorys的url
router.register(r'categories', CategoryViewSet, basename='categories')

# 配置发送验证码的url
router.register(r'code', SmsCodeViewSet, basename='code')

# 配置users的viewSet
router.register(r'users', UserViewSet, basename='users')

# 配置user_operation的收藏viewSet
router.register(r'userfavs', UserFavViewset, basename='userfavs')

# 配置user_operation的留言viewSet
router.register(r'messages', LeavingMessageViewset, basename='messages')

# 收货地址
router.register(r'address', AddressViewset, basename='address')

# 购物车
router.register(r'shopcarts', ShoppingCartVeiwset, basename='shopcarts')

# 订单相关url
router.register(r'orders', OrderViewset, basename='orders')


urlpatterns = [
       # path('api-auth/', include('rest_framework.urls')),
       path('xadmin/', xadmin.site.urls),
       re_path('media/(?P<path>.*)$', serve, {"document_root": MEDIA_ROOT}),
       # 商品列表页
       path(r'',include(router.urls)),

       # drf自带的token认证模式
       url(r'^api-token-auth/', views.obtain_auth_token),

       # jwt的认证模式
       url(r'^login/', obtain_jwt_token),
       path('docs/', include_docs_urls(title="慕学生鲜")),
]
